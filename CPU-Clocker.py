import time
import os


print("--CPU Clocker (specifically made for Intel i3-8130U. Please read the README.)--")

def menu():
    print("---Main Menu---")
    print("1. neofetch")
    print("2. Set CPU to 400MHz (power saver)")
    print("3. Set CPU to 1GHz (low)")
    print("4. Set CPU to 2.2GHz (medium)")
    print("5. Set CPU to 3GHz (high)")
    print("6. Set CPU to 3.4GHz (turbo)")
    print("7. Quit")
    option = int(input("Please choose an option: "))
    return option

option = menu()

if option == 1:
    os.system("clear")
    os.system("neofetch")

elif option == 2:
    os.system("clear")
    print("[*] Setting maximum CPU speed to 400MHz... This will significantly reduce the performance of your machine and should probably be used in text only environment but save power.")
    os.system("sudo cpupower frequency-set -u 400MHz")
    print("[*] All Done! Exiting...")
    quit()

elif option == 3:
    os.system("clear")
    print("[*] Setting maximum CPU speed to 1GHz... This will only run basic tasks but save power.")
    os.system("sudo cpupower frequency-set -u 1GHz")
    print("[*] All Done! Exiting...")
    quit()

elif option == 4:
    os.system("clear")
    print("[*] Setting maximum CPU speed to 2.2GHz... This is your CPUs base speed and can be used for everyday use")
    os.system("sudo cpupower frequency-set -u 2.2GHz")
    print("[*] All Done! Exiting...")
    quit()

elif option == 5:
    os.system("clear")
    print("[*] Setting maximum CPU speed to 3GHz... This is high power and can use a lot of battery power.")
    os.system("sudo cpupower frequency-set -u 3GHz")
    print("[*] All Done! Exiting...")
    quit()

elif option == 6:
    os.system("clear")
    print("[*] Setting maximum CPU speed to 3.4GHz... This is your CPUs turbo speed and is the maximum safe/stable speed.")
    os.system("sudo cpupower frequency-set -u 3.4GHz")
    print("[*] All Done! Exiting...")
    quit()

else:
    print("[!] Error... Exiting")
    quit()